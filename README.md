# vizcab-project

Projet effectué dans le cadre du test technique frontend de Vizcab. La pagination est mise en place (5 résultats par page). Également en bonus, des champs de recherche permettent de trouver plus facilement un salarié.

J'utilise le framework Buefy pour l'affichage du tableau, et le plugin node-fetch pour récupérer les données de l'API.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
